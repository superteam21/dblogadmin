describe('baseValidation', () => {

    afterEach(() => {
        let inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        inputs = inputs.map(elem => elem.value = null);
    });

    it('should return boolean result of validation', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db';
        table.value = 'table';
        login.value = 'login';
        pass.value = 'pass';

        const actual = typeof baseValidation();

        const expected = 'boolean';
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if not all of inputs contain data', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db';
        table.value = 'table';
        login.value = 'login';
        pass.value = null;

        const actual = baseValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if inputs contents consist of 1 symbol', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'd';
        table.value = 't';
        login.value = 'l';
        pass.value = 'p';

        const actual = baseValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if inputs content consist of more then 20 symbols', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db'.padEnd(21, 'a');
        table.value = 'table'.padEnd(21, 'a');
        login.value = 'login'.padEnd(21, 'a');
        pass.value = 'pass'.padEnd(21, 'a');


        const actual = baseValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return "true" if inputs contain Latin symbols (lowercase)', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db';
        table.value = 'table';
        login.value = 'login';
        pass.value = 'pass';

        const actual = baseValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return "true" if inputs contain Latin symbols (uppercase)', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'DB';
        table.value = 'TABLE';
        login.value = 'LOGIN';
        pass.value = 'PASS';

        const actual = baseValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if inputs contain only digits', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 1111;
        table.value = 2222;
        login.value = 3333;
        pass.value = 4444;

        const actual = baseValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if inputs contain digits and first symbol is letter', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'a1';
        table.value = 'a2';
        login.value = 'a3';
        pass.value = 'a4';

        const actual = baseValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return "true" if inputs include "-"', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db-';
        table.value = 'table-';
        login.value = 'login-';
        pass.value = 'pass-';

        const actual = baseValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return "true" if inputs include "_"', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db_';
        table.value = 'table_';
        login.value = 'login_';
        pass.value = 'pass_';

        const actual = baseValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if inputs include other special symbols', () => {
        const inputs = [...document.forms.loginForm.elements];
        inputs.pop();
        let [db, table, login, pass] = inputs;
        db.value = 'db=';
        table.value = 'table+';
        login.value = 'login?';
        pass.value = 'pass>';

        const actual = baseValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

});

describe('renderTableRows', () => {

    afterEach(() => {

        if (document.getElementById('editorList')) {
            document.getElementById('editorList').remove();
        }

    });

    it('should add new ul into #editor', () => {
        const testRow = {
            id: "123",
            searchValue: "Vasya",
        };
        const testServerResponse = new Array(4).fill(testRow);
        renderTableRows(testServerResponse);

        const actual = document.getElementById('editor').firstElementChild.tagName;

        const expected = 'UL';
        assert.strictEqual(actual, expected);
    });

    it('should add new li with row data into ul', () => {
        const testRow = {
            id: "123",
            searchValue: "Vasya",
        };
        const testServerResponse = new Array(4).fill(testRow);
        renderTableRows(testServerResponse);
        const childrenArray = [...document.getElementById('editorList').children];

        const actual = childrenArray.map(elem => elem.tagName);

        const expected = ['LI', 'LI', 'LI', 'LI'];
        assert.deepStrictEqual(actual, expected);
    });

    it('should add p and input into li', () => {
        const testRow = {
            id: "123",
            searchValue: "Vasya",
        };
        const testServerResponse = new Array(4).fill(testRow);
        renderTableRows(testServerResponse);
        const childrenArray = [...document.getElementById('editorList').firstElementChild.children];

        const actual = childrenArray.map(elem => elem.tagName);

        const expected = ['P', 'INPUT'];
        assert.deepStrictEqual(actual, expected);
    });

    it('p.textContent should be defined by searchValue', () => {
        const testRow = {
            id: "123",
            searchValue: "Vasya",
        };
        const testServerResponse = new Array(4).fill(testRow);
        renderTableRows(testServerResponse);
        const liArray = [...document.getElementById('editorList').children];
        const textValues = liArray.map(elem => elem.firstElementChild.textContent);

        const actual = textValues.every((elem, index) => elem === testServerResponse[index].searchValue);

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should not add new ul if argument is undefined', () => {
        const testServerResponse = undefined;
        renderTableRows(testServerResponse);

        const actual = document.getElementById('editor').firstElementChild.tagName === 'UL';

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should delete exists list', () => {
        const container = document.getElementById('editor');
        const list = document.createElement('ul');
        list.className = 'list';
        container.prepend(list);
        const testRow = {
            id: "123",
            searchValue: "Vasya",
        };
        const testServerResponse = new Array(4).fill(testRow);
        renderTableRows(testServerResponse);
        const children = [...container.children]

        const actual = children.map(elem => elem.tagName);

        const expected = ['UL', 'FORM'];
        assert.deepStrictEqual(actual, expected);
    });

});

describe('editValidation', () => {
    let sandbox = null;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements.filter(elem => elem.tagName === 'TEXTAREA').map(item => item.value = null);

        if (sandbox) {
            sandbox.restore();
        }
    });

    it('should return boolean result of validation', () => {
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements.filter(elem => elem.tagName === 'TEXTAREA').map(item => item.value = 'test');

        const actual = typeof editValidation();

        const expected = 'boolean';
        assert.strictEqual(actual, expected);
    });

    it('should return "false" if not all of inputs contain data', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'male',
            address: null,
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return true if all fields are in conformity with the terms', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "age" contains number as string ("25")', () => {
        const testData = {
            name: 'Vasya',
            age: '25',
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return false if field "age" value is not a number', () => {
        const testData = {
            name: 'Vasya',
            age: 'faefb',
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return false if field "age" value is more than 150', () => {
        const testData = {
            name: 'Vasya',
            age: 200,
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return false if field "age" value is less than 0', () => {
        const testData = {
            name: 'Vasya',
            age: -200,
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "gender" contains "male" (lowercase)', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "gender" contains "MALE" (uppercase)', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'MALE',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "gender" contains "Male" (capital)', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'Male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "gender" contains "female" (lowercase)', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'female',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "gender" contains "FEMALE" (uppercase)', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'FEMALE',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return true if field "gender" contains "Female" (capital)', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'Female',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = true;
        assert.strictEqual(actual, expected);
    });

    it('should return false if field gender value is not equal to "Male" or "Female"', () => {
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'dfbsfn',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        const actual = editValidation();

        const expected = false;
        assert.strictEqual(actual, expected);
    });

    it('should call showError func if result of gender validation is false', () => {
        const stub = sandbox.stub(window, 'showError');
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'dfbsfn',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        editValidation();

        sandbox.assert.calledOnce(stub);
        sandbox.assert.calledWith(stub, 'Error in "GENDER". Valid values are "male" or "female"');
    });

    it('should call showError func if result of age validation is false', () => {
        const stub = sandbox.stub(window, 'showError');
        const testData = {
            name: 'Vasya',
            age: 250,
            gender: 'male',
            address: 'm. Bila Tserkva Kyiv',
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        editValidation();

        sandbox.assert.calledOnce(stub);
        sandbox.assert.calledWith(stub, 'Error in "AGE". Value must be an integer between 0 and 150');
    });

    it('should call showError func if not all of inputs contain data', () => {
        const stub = sandbox.stub(window, 'showError');
        const testData = {
            name: 'Vasya',
            age: 25,
            gender: 'male',
            address: null,
        };
        const editElements = [...document.forms.editForm.elements];
        const inputFields = editElements
            .filter(elem => elem.tagName === 'TEXTAREA')
            .map((item, index) => item.value = Object.values(testData)[index]);

        editValidation();

        sandbox.assert.calledOnce(stub);
        sandbox.assert.calledWith(stub, 'Error. All fields are required');
    });
});
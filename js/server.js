const http = require('http');
const url = require('url');
const mysql = require('mysql2');
const {searchValue} = require('../config');
let variant;
let connection;


http.createServer((request, response) => {
    const pathName = url.parse(request.url, true).pathname;
    const [, version, db_table, id] = pathName.split('/');
    variant = version;
    let body;
    let sql;

    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.setHeader("Content-Type", "application/json, text/plain; charset=utf-8;");
    response.setHeader('Access-Control-Max-Age', '-1');
    response.statusCode = 200;

    if (request.method === 'POST') {
        body = [];
        request.on('data', (chunk) => {
          body.push(chunk);
        }).on('end', () => {
            body = JSON.parse(body);
            if (body) {
                const [database, , user, password] = body;
            connection = mysql.createConnection({
                host: "localhost",
                user: user,
                database: database,
                password: password
            });
            connection.connect(function(err) {
                if (err) {
                    response.statusCode = 500;
                    response.write(JSON.stringify(err));
                    response.end();
                }
            
                response.write(JSON.stringify("Connected!"))
                response.end();            
            });       
            } else {
                response.write(JSON.stringify('Connection is closed'));
                response.end();
                connection.end();
            }
             
        });
    }

    if (request.method === 'GET') {
        if (db_table === 'person') {
            sql = `SELECT id, ${version} FROM person`;
            connection.query(sql, function (err, result) {
                if (err) { 
                    response.statusCode = 500;
                    response.write(JSON.stringify(err));
                    response.end();
                }

                const resultData = parseData(result);
                response.write(JSON.stringify(resultData));
                response.end();
            });
        }
    }
}).listen(8080);

function parseData(data) {
    const newData = [];
    data.forEach(element => {
        newData.push({id: element.id, searchValue: JSON.parse(element[variant])[searchValue]});
    });
    return newData
}


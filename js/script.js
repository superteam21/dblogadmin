const firstPageSubmit = document.getElementById('firstPageSubmit');
const tabsetHeader = document.getElementById('tabsetHeader');
const buttonLogs = document.getElementById('buttonLogs');
const selectElement = document.querySelector('.header__select');
const editContainer = document.getElementById('editContainer');
const logOutBtn = document.querySelector('.header__button');
let currentListRows;
const URL = 'http://localhost:8080';
let version = 'variant1';


firstPageSubmit.addEventListener('click', async (event) => {
    event.preventDefault();
    let errorMsg;

    if (baseValidation()) {

        try {
            const connection = await connectWithDataBase();
            if (connection.code == 'ER_ACCESS_DENIED_ERROR') {
                throw new Error(connection.code);
            } else {
                const response = await requestJson(`${URL}/${version}/person`);
                renderTableRows(response);
                currentListRows = response;
                const mainContainer = document.getElementById('mainContainer');
                const firstPageContainer = document.getElementById('firstPageContainer');
                mainContainer.style.display = 'flex';
                firstPageContainer.style.display = 'none';
            }        
        } catch (err) {
            err = 'Internal Server Error';
            showError(err);
        }

    } else {
        errorMsg = 'Incorrect data entered';
        showError(errorMsg);
    }
});

async function connectWithDataBase() {
    const inputs = [...document.forms.loginForm.elements];
    const formData = inputs
        .filter(item => item.getAttribute('type') !== 'submit')
        .map(elem => elem.value);
    
    return await requestJson(URL, 'POST', JSON.stringify(formData));
}

tabsetHeader.addEventListener('click', selectWindow);
editContainer.addEventListener('click', selectBtn);
selectElement.addEventListener('change', onSelectElementChange);
logOutBtn.addEventListener('click', onLogOutBtn);

function selectBtn(event) {
    inputType = event.target.id;

    switch (inputType) {
        case 'save':
           document.forms.editForm.onsubmit = postChange;
            break;
        case 'cancel':
            closeEditor();
            break;
    }
}

async function onSelectElementChange(event) {
    version = event.target.value;
    try {
        const response = await requestJson(`${URL}/${version}/person`);
        renderTableRows(response);
        currentListRows = response;    
    } catch (err) {            
        err = 'Internal Server Error';
        showError(err);
    }
}

async function onLogOutBtn() {
    try {
        await requestJson(URL, 'POST', JSON.stringify(''));
        mainContainer.style.display = 'none';
        firstPageContainer.style.display = 'flex';
        selectElement.firstElementChild.selected = true;
        version = 'variant1';
    } catch (err){
        err = 'Internal Server Error';
        showError(err);
    }
}

//*******************  EDIT BUTTONs *********************

async function postChange(event) {
    event.preventDefault();

    let body = {
        id: 123132,
        row:{
            name:       document.getElementById('name').nodeValue,
            age:        document.getElementById('age').nodeValue,
            gender:     document.getElementById('gender').nodeValue,
            address:    document.getElementById('address').nodeValue,
        },
    };

    let validation = true;// TODO <<--- заглушка
    if (validation){ //TODO <<--- подставить функцию проверки корректности ввода данных
        // const change = await requestJson(`${URL}/${version}/person`, 'PUT', body);

        let change = '200';
        // if (change.status === '200'){
        if (change === '200'){ // запись изменена и отправлена в БД
            // if (change === '501'){ // запись не изменена

            // заменить на editContainer.innerHTML = null; TODO
            // <<--- это бы не плохо делать в функции генерации блоков, а фактически перерисовки
            let editorDivs = document.getElementsByClassName('edit-container__item');
            for (let index = 0; index < editorDivs.length;){
                editorDivs[index].remove();
            }
            //<<--- END

            closeEditor();
            renderTableRows(currentListRows);
        } else {
            showError(`Запись не изменена!`);
        }
    }
    return false;
}


function closeEditor() { // закрываем форму редактирования (форма 3) без сохранения измененний
    const editorList = document.getElementById('editorList');
    const editContainer = document.getElementById('editContainer');

    editorList.style.display = 'block';
    editContainer.style.display = 'none';
    buttonLogs.disabled = false;
    selectElement.disabled = false;
}

//*******************  EDIT BUTTONs END ******************


function baseValidation() {
    const loginValidExp = /^[a-zA-Z]+[a-zA-Z0-9_-]{1,20}$/;

    const inputs = [...document.forms.loginForm.elements];
    const formData = inputs
        .filter(item => item.getAttribute('type') !== 'submit')
        .map(elem => elem.value);

    return formData.every(elem => loginValidExp.test(String(elem)) && elem.length <= 20);
}

function showError(errorMessage) {
    const warningText = document.getElementById('warningText');
    warningText.textContent = errorMessage;
    warningText.style.visibility = 'visible';
    setTimeout("warningText.style.visibility = 'hidden'", 3000);
}

async function requestJson(url, method = 'GET', body = null) {
    try {
        const res = await fetch(url, {method, body});
        return await res.json();
    } catch (err) {
        throw err;
    }
}

function selectWindow(event) {
    const tabContent = document.getElementsByClassName("tabset__content");

    for (let i = 0; i < tabsetHeader.children.length; i++) {
        tabsetHeader.children[i].classList.remove('active');
    }

    event.target.classList.add('active');

    for (let i = 0; i < tabContent.length; i++) {
        tabContent[i].classList.add('hidden')
    }

    document.getElementById(event.target.value).classList.remove('hidden');
}

function renderTableRows(serverResponse) {

    if (!serverResponse) {
        return void 0;
    }

    const editorElements = [...document.getElementById('editor').children];
    const existList = editorElements.find(elem => elem.className === 'list');

    if (existList) {
        existList.remove();
    }

    const ul = document.createElement('ul');
    ul.className = "list";
    ul.setAttribute('id', 'editorList');

    for (let row of serverResponse) {
        const li = document.createElement('li');
        li.className = 'list__item';
        li.dataset.id = row.id;

        const text = document.createElement('p');
        text.className = 'list__text';
        text.textContent = row.searchValue;

        const btn = document.createElement('input');
        btn.setAttribute('type', 'submit');
        btn.setAttribute('value', 'Change');
        btn.className = 'list__button';

        li.append(text);
        li.append(btn);
        ul.append(li);
    }

    document.getElementById('editor').prepend(ul);

    const editorList = document.getElementById('editorList');
    editorList.addEventListener('click', openEditForm);

}

function openEditForm(event) {
    const target = event.target;
    const editContainer = document.getElementById('editContainer');

    buttonLogs.disabled = true;
    selectElement.disabled = true;

    if (target.className === 'list__button') {
        target.closest('#editorList').style.display = 'none';
        editContainer.style.display = 'block';
        editContainer.innerHTML = null;
        generateList(keyData);
    }

    return false;
}

const keyData = {
    id: 123132,
    row: {
        name: 'Катерина',
        age: 28,
        gender: 'female',
        address: 'вулиця Любарського, 14, Дніпро',
    }
};

function generateList(data) {
    const editContainer = document.getElementById('editContainer');
    const objectValue = Object.values(data.row);
    const objectKey = Object.keys(data.row);

    for (let index = 0; index < objectValue.length; index++) {

        const div = document.createElement('div');
        div.className = 'edit-container__item';

        const label = document.createElement('label');
        label.setAttribute('for', 'objectKey(index)');
        label.textContent = objectKey[index];

        const textarea = document.createElement('textarea');
        textarea.setAttribute('name', 'objectKey(index)');
        textarea.id = objectKey[index];
        textarea.value = objectValue[index];

        editContainer.append(div);
        div.append(label);
        div.append(textarea);
    }

    const div = document.createElement('div');
    div.className = 'edit-buttons';

    const inputSave = document.createElement('input');
    inputSave.setAttribute('type', 'submit');
    inputSave.className = 'edit-buttons__button';
    inputSave.value = 'Save';
    inputSave.id = 'save';

    editContainer.append(div);
    div.append(inputSave);

    const inputCancel = document.createElement('input');
    inputCancel.setAttribute('type', 'button');
    inputCancel.className = 'edit-buttons__button';
    inputCancel.value = 'Cancel';
    inputCancel.id = 'cancel';

    div.append(inputCancel);
}

function editValidation() {
    const editForm = document.forms.editForm;
    let errorMsg = '';

    const editElements = [...editForm.elements];
    const inputFields = editElements.filter(elem => elem.tagName === 'TEXTAREA');

    for (let field of inputFields) {

        if (!field.value) {
            errorMsg = 'Error. All fields are required';
        }

        switch (field.name) {
            case 'age':
                const ageValue = field.value;

                if (!Number.isFinite(Number(ageValue)) || ageValue < 0 || ageValue > 150) {
                    errorMsg = 'Error in "AGE". Value must be an integer between 0 and 150';
                }

                break;

            case 'gender':
                const genderValue = field.value;
                const genderExpMale = /^male$/i;
                const genderExpFemale = /^female$/i;

                if (!genderExpMale.test(genderValue) && !genderExpFemale.test(genderValue)) {
                    errorMsg = 'Error in "GENDER". Valid values are "male" or "female"';
                }

                break;

            default:
                break;

        }

    }

    if (!errorMsg) {
        return true
    } else {
        showError(errorMsg);
        return false;
    }

}